# Patterns Dynamics Security Monitor IAM configuration
To enable integration with Patterns Dynamics Security Monitor you need to provide `arn` of read-only role that allows synchronizing DNS configuration and other AWS assets.
You can do this manually or simply by using provider terraform configuration file `iam.tf`.

## Manual configuration
![](assets/README-82744987.png)

### Role policy:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PatternsDynamicsMonitorRO",
            "Effect": "Allow",
            "Action": [
                "s3:listbucket",
                "s3:listallmybuckets",
                "s3:getbucketwebsite",
                "s3:getbucketversioning",
                "s3:getbuckettagging",
                "s3:getbucketpolicy",
                "s3:getbucketnotification",
                "s3:getbucketlogging",
                "s3:getbucketlocation",
                "s3:getbucketcors",
                "s3:getbucketacl",
                "route53domains:List*",
                "route53domains:Get*",
                "ec2:DescribeSnapshots",
                "ec2:DescribeInstances",
                "ec2:DescribeImages",
                "elasticloadbalancing:DescribeLoadBalancers",
                "cloudtrail:gettrailstatus",
                "cloudtrail:describetrails"
            ],
            "Resource": "*"
        }
    ]
}
```

### Role trust relationship:
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PatternsDynamicsMonitor",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::362977498811:root"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```
