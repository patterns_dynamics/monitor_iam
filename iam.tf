data "aws_iam_policy_document" "patterns_dynamics_monitor" {
  statement {
    sid    = "PatternsDynamicsMonitorRO"
    effect = "Allow"

    actions = [
      "cloudtrail:describetrails",
      "cloudtrail:gettrailstatus",
      "route53:Get*",
      "route53:List*",
      "ec2:DescribeSnapshots",
      "ec2:DescribeImages",
      "ec2:DescribeInstances",
      "elasticloadbalancing:DescribeLoadBalancers",
      "s3:getbucketacl",
      "s3:getbucketcors",
      "s3:getbucketlocation",
      "s3:getbucketlogging",
      "s3:getbucketnotification",
      "s3:getbucketpolicy",
      "s3:getbuckettagging",
      "s3:getbucketversioning",
      "s3:getbucketwebsite",
      "s3:listbucket",
      "s3:listallmybuckets",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "patterns_dynamics_monitor" {
  name = "PatternsDynamicsMonitor"

  policy = "${data.aws_iam_policy_document.patterns_dynamics_monitor.json}"
}

data "aws_iam_policy_document" "patterns_dynamics_monitor_assume_role" {
  statement {
    sid = "PatternsDynamicsMonitor"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::362977498811:root"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "patterns_dynamics_monitor" {
  name = "patterns_dynamics_monitor"

  assume_role_policy = "${data.aws_iam_policy_document.patterns_dynamics_monitor_assume_role.json}"
}

resource "aws_iam_policy_attachment" "patterns_dynamics_monitor" {
  name       = "patterns_dynamics_monitor"
  roles      = ["${aws_iam_role.patterns_dynamics_monitor.name}"]
  policy_arn = "${aws_iam_policy.patterns_dynamics_monitor.arn}"
}

output "role_arn" {
  value = "${aws_iam_role.patterns_dynamics_monitor.arn}"
}
